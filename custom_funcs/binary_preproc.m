function [trials, y, trialnum, filenum] = binary_preproc(data, num_chn, trig_ids, start_id, window, winhop, freqbands, fodr, fs)
% Performs preprocessing on EEG data (filters and cubify)
%
% INPUTS
%   data        EEG data
%   num_chn     Number of channels to extract
%   trig_ids    Triggers to look for
%   window      length of the sliding winndow in sec
%   hops        offsets to slide the window
%   f_a         alpha band limits
%   f_b         beta band limints
%   fs          sampling freq.
%   fodr        filter order
%
% OUTPUTS
%   trials_x    cubified and filtered data
%   t           timestamps of samples relative to trigger

global trial_count;
trial_count = 0;

num_files = length(data);

[Nbands, ~] = size(freqbands);

B = zeros(Nbands, 2*fodr+1);
A = zeros(Nbands, 2*fodr+1);
for i = 1:Nbands
    [B(i, :), A(i, :)] = butter(fodr, freqbands(i,:)/(fs/2));
end

hopnum = winhop * fs;

N = (window(2) - window(1))*fs;
%M = length(data(1).pos)/4;

trials = [];
y = [];
trialnum = [];
filenum = [];

for filedx = 1:num_files
    trig_x_pos = data(filedx).pos( (data(filedx).typ == trig_ids(1)) | (data(filedx).typ == trig_ids(2)));
    %trig_2_pos = data(filedx).pos(data(filedx).typ == trig_ids(2));
    trig_s_pos = data(filedx).pos(data(filedx).typ == start_id);
    
    trig_x_typ = data(filedx).typ;
    trig_x_typ( (trig_x_typ ~= trig_ids(1)) & (trig_x_typ ~= trig_ids(2)) ) = [];
    
    waves_tc = data(filedx).waves(:, 1:num_chn);
    waves_tc(isnan(waves_tc)) = 0;
    
    waves = zeros([size(waves_tc), Nbands]);
    for i = 1:Nbands
        waves(:,:,i) = filter(B(i,:), A(i,:), waves_tc);
    end

    [trials1, y1, trialnum1] = extract_cube(waves, num_chn, trig_x_pos, trig_x_typ, trig_s_pos, N, hopnum, 10 / winhop);
    %[trials_f2, trials_a2, trials_b2] = extract_cube(waves_f, waves_a, waves_b, num_chn, trig_2_pos, trig_s_pos, N, hopnum, 200);
    
    trials = cat(3, trials, trials1);
    y = [y;y1];
    trialnum = [trialnum; trialnum1];
    filenum = [filenum; filedx*ones(length(trialnum1),1)];
end


end

% Helper function
function [trials, y, trialnum] = extract_cube(waves, num_chn, trig_x_pos, trig_x_typ, trig_s_pos, N, hopnum, maxhops)

global trial_count;

[~, ~, Nbands] = size(waves);

trials = [];
y = [];
trialnum = [];

for i = 1:Nbands
    trials_p = zeros(N, num_chn, 0);
    for trialdx = 1:length(trig_x_pos) % iterate over all the triggers
        next_start = trig_s_pos(find(trig_s_pos > trig_x_pos(trialdx), 1));
        if isempty(next_start)
            next_start = length(waves(:,:,i)) + 1;
        end
        hops = 0:hopnum: (next_start-trig_x_pos(trialdx)-N);
        if( length(hops) > maxhops )
            hops = hops(1:maxhops);
        end

        rowdx = [0:N-1]' + trig_x_pos(trialdx) + hops(:)';

        trials_p = cat( 3, trials_p, permute(reshape(waves(rowdx(:), 1:num_chn, i)', num_chn, N, []), [2, 1, 3]) );
        if i == 1 % only generate y for the first one (same for all)
            y = [y;ones(length(hops),1) * trig_x_typ(trialdx)];
            trialnum = [trialnum;ones(length(hops),1) * trial_count];
            trial_count = trial_count + 1;
        end
        
        

    end
    trials = cat(4, trials, trials_p);
end
end