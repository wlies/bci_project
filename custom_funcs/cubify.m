function [waves_tr, t] = cubify(waves_t, tr_idx, t_bounds, t_hops, fs)
% Takes a continuous EEG signal and breaks it up into pieces around 
% trigger locations
%
%   waves_t         EEG signal
%   tr_idx          Indices of the triggers
%   t_bounds        1x2 matrix with start/end times relative to trigger at
%                       t=0. These are the window bounds
%   t_hops          Sliding window offsets for each given trigger. Set
%                       to 0 to get only 1 window from each trigger
%   fs              Sampling rate (Hz)

    t_strt = t_bounds(1);
    t_end = t_bounds(2);
    N = (t_end - t_strt)*fs;
    idxs = [0:N-1]' + tr_idx(:)' + t_strt*fs; %indices for each given window
    
    idxs_hops = idxs + permute(t_hops(:)*fs, [3,2,1]);
    
    waves_tr = waves_t(idxs_hops);
    t = [0:N-1]'/fs + t_strt;
end