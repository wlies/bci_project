function [waves_tc] = filter_CAR(waves_tc, num_chn)
%Apply CAR filter. Assume channel signals are in columns
waves_tc(:, 1:num_chn) = waves_tc(:, 1:num_chn) - mean(waves_tc(:, 1:num_chn), 2);