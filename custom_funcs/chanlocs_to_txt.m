function fd_txt = chanlocs_to_txt(folder_chanlocs, file_chanlocs)

locs = load([folder_chanlocs, file_chanlocs]);

x = extractfield(locs.chanlocs16, 'X')';
y = extractfield(locs.chanlocs16, 'Y')';
z = extractfield(locs.chanlocs16, 'Z')';

[th r] = cart2topo(x,y,z);