function [feats, y, feat_descr] = feat_ent(waves_tthc, y, descr)
% computes the power of each column

    [N, ~, ~, ~] = size(waves_tthc);

    feats = fft(waves_tthc, [], 1);
    feats = abs(feats(1:end/2, :,:)).^2;
    feats = feats ./ sum(feats, 1);
    feats = permute(-sum(feats .* log2(feats), 1), [2 3 4 1]) / log2(N/2);
    
    feats = feats';
    
    
    [~, num_feat] = size(feats);
    feat_descr = string(zeros(num_feat, 1));
    for i = 1:num_feat
        feat_descr(i) = [descr, int2str(i)];
    end
end