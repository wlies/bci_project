function [feats, y, feat_descr] = feat_pwr(waves_tthc, y, descr)
% computes the power of each column

    feats = permute(sum(waves_tthc.^2, 1), [2 3 4 1]);
    feats = feats';
    
    [~, num_feat] = size(feats);
    feat_descr = string(zeros(num_feat, 1));
    for i = 1:num_feat
        feat_descr(i) = [descr, int2str(i)];
    end
end