function [feats, y, feat_descr] = feat_ent(waves_tthc, y, t, descr)
% computes the change in entropy before/after trigger
    beforedx = find(t < 0);
    afterdx = find(t >= 0);

    [feats_before, y, feat_descr] = feat_ent(waves_tthc(beforedx,:,:), y, descr);
    [feats_after, ~, ~] = feat_ent(waves_tthc(afterdx,:,:), y, descr);
    
    feats = feats_after - feats_before;
end