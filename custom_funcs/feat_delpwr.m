function [feats, y, feat_descr] = feat_delpwr(waves_tch, y, t, descr)
% computes the change in power before/after trigger
    beforedx = find(t < 0);
    afterdx = find(t >= 0);

    feats = squeeze(sum(waves_tch(afterdx,:,:).^2, 1)) ...
                - squeeze(sum(waves_tch(beforedx,:,:).^2, 1));
    feats = feats';
    
    [~, num_feat] = size(feats);
    feat_descr = string(zeros(num_feat, 1));
    for i = 1:num_feat
        feat_descr(i) = [descr, int2str(i)];
    end
end