% Homework 2
% William Lies

% This script loads all the EEG signals and generates labelled data for
%   each session.

clear;


%% Define a structure to put everything
session(1).name = 'CR09NA';
session(1).type = 'Offline';
session(1).triggers = [770, 771];
session(2).name = 'CR09NA';
session(2).type = 'S2';
session(2).triggers = [770, 771];
session(3).name = 'CR09NA';
session(3).type = 'S3';
session(3).triggers = [770, 771];

session(4).name = 'ES31OS';
session(4).type = 'Offline';
session(4).triggers = [770, 771];
session(5).name = 'ES31OS';
session(5).type = 'S2';
session(5).triggers = [770, 771];
session(6).name = 'ES31OS';
session(6).type = 'S3';
session(6).triggers = [770, 771];

session(7).name = 'LE04VE';
session(7).type = 'Offline';
session(7).triggers = [769, 770];
session(8).name = 'LE04VE';
session(8).type = 'S2';
session(8).triggers = [769, 770];
session(9).name = 'LE04VE';
session(9).type = 'S3';
session(9).triggers = [769, 770];

session(10).name = 'MA29ON';
session(10).type = 'Offline';
session(10).triggers = [769, 771];
session(11).name = 'MA29ON';
session(11).type = 'S2';
session(11).triggers = [769, 771];
session(12).name = 'MA29ON';
session(12).type = 'S3';
session(12).triggers = [769, 771];

%% Do the processing
% Add biosig toolbox
addpath(genpath('./p_files/ToolBoxes/biosigToolBox/biosig'));
addpath(genpath('./p_files/ToolBoxes/eeglab2020_0/'));
addpath('custom_funcs/');

for sesidx = 1:length(session)
    disp([session(sesidx).name, sprintf('\t'), session(sesidx).type]);

    % Find all the data that we need
    %offlists = dir('../project/p_files/Project_1/MITraining/*/Offline/*.gdf');
    offlists = dir(['./p_files/Project_1/MITraining/', session(sesidx).name, '/', session(sesidx).type,'/*.gdf']);
    num_files = length(offlists);

    %data = struct(
    num_chn = 16;

    % The data struct contains the waves and triggers for all offline trials
    clear data;
    for idx = 1:num_files
        [data(idx).waves, meatdata] = sload([offlists(idx).folder, '/', offlists(idx).name]);
        data(idx).waves = filter_CAR(data(idx).waves, num_chn);
        data(idx).typ = meatdata.EVENT.TYP;
        data(idx).pos = meatdata.EVENT.POS;
        data(idx).fs = meatdata.EVENT.SampleRate;

        data(idx).trigger_types = [ (~isempty(find(data(idx).typ == 769, 1))), ...
                                    (~isempty(find(data(idx).typ == 770, 1))), ...
                                    (~isempty(find(data(idx).typ == 771, 1))) ];
    end

    fs = data(1).fs; %assume fs is constant for all files (it is)

    window = [-1,1]; %look from 0 to 6 seconds after trigger, split up later
    t = window(1):1/fs:window(2)-1/fs;
    winhop = 1/8; % Time between start of successive windows

    freqbands = [8, 11; ... %alpha
                 12, 15;
                 16, 19;
                 20, 23; %beta
                 24, 27;
                 28, 31;
                 8, 32];
             
    band_descr = ["8-11 Hz"; "12-15 Hz"; "16-19 Hz"; "20-23 Hz"; "24-27 Hz"; "28-31 Hz"; "8-32 Hz"];
    
    [Nbands, ~] = size(freqbands);
    
    fodr = 5; %filter order

    classes = session(sesidx).triggers;
    start_trigger = 1; % Each trial starts with trigger 1

    [trials, y_h, trialnum, filenum] = binary_preproc(data, num_chn, classes, start_trigger, window, winhop, freqbands, fodr, fs);

    % trials_x is a 3-D array: (samples x channels x trials)

    %% Put the features in the matrix X
    
    firstwind = logical([1;diff(trialnum)]);
    Ntrials = trialnum(end) +1;
    
    [Nwsamp, ~, Nwindow, ~] = size(trials);
    X = zeros(Nwindow, 0);
    feat_descr = [];
    
    postimedx = find(t >= 0); % indices starting with the trigger
    pretimedx = find(t >= 0); % indices starting with the trigger
    
    for i = 1:Nbands
        [x_p, y, fd_p] = feat_pwr(trials(postimedx,:,:,i), y_h, [char(band_descr(i)), ' Power Ch ']);
        X = [X, x_p];
        feat_descr = [feat_descr; fd_p];
    end
    
    for i = 1:Nbands
        [x_p2, ~, fd_p] = feat_pwr(trials(postimedx,:,:,i), y_h, [char(band_descr(i)), ' Power Change Ch ']);
        [x_pt, ~, ~] = feat_pwr(trials(pretimedx,:,firstwind,i), y_h, [char(band_descr(i)), ' ']);
        x_p1 = zeros(size(x_p2));
        for ii = 0:(Ntrials-1)
            x_p1(trialnum == ii, :) = repmat(x_pt(ii+1, :), sum(single(trialnum==ii)), 1);
        end
    
        X = [X, (x_p2 - x_p1)];
        feat_descr = [feat_descr; fd_p];
    end
    
    [x_p, y, fd_p] = feat_ent(trials(postimedx,:,:,7) , y_h, 'Entropy Ch ');
    X = [X, x_p];
    feat_descr = [feat_descr; fd_p];
    [x_p2, ~, fd_p] = feat_ent(trials(postimedx,:,:,7) , y_h, 'Entropy Change Ch ');
    [x_pt, ~, ~] = feat_ent(trials(pretimedx,:,firstwind,7) , y_h, ' ');
    x_p1 = zeros(size(x_p2));
    for i = 0:(Ntrials-1)
        x_p1(trialnum == i, :) = repmat(x_pt(i+1, :), sum(single(trialnum==i)), 1);
    end
        
    X = [X, (x_p2 - x_p1)];
    feat_descr = [feat_descr; fd_p];

    %Compute fisher scores
    n1 = sum( y == classes(1) );
    n2 = sum( y == classes(2) );
    X1 = X( y==classes(1), :);
    X2 = X( y==classes(2), :);
    fishers = (n1*(mean(X1, 1) - mean(X,1)).^2  +  n2*(mean(X2, 1) - mean(X,1)).^2) ./ (n1*var(X1,0,1) + n2*var(X2,0,1));

    %if strcmp(session(sesidx).type, 'Offline')
        [sorted, indices] = sort(fishers, 'descend');
    %end

    places = 5;
    for i = 1:places
        disp([int2str(i), ': ', sprintf('%.4f    ', sorted(i)), char(feat_descr(indices(i)))]);
    end
    
    session(sesidx).results.y = y;
    session(sesidx).results.X = X;
    session(sesidx).results.X1 = X1;
    session(sesidx).results.X2 = X2;
    session(sesidx).results.feat_descr = feat_descr;
    session(sesidx).results.fishers = fishers;
    session(sesidx).results.indices = indices;
    
    y_bin = double(y == y(1));
    y_bin(y_bin == 0) = -1;
    
    % Write data to CSV files
    outC = [[cellstr(feat_descr(indices))' ; num2cell(X(:,indices))], [cellstr('Label'); num2cell(y)], [cellstr('Binary Label'); num2cell(y_bin)], [cellstr('Trial Index'); num2cell(trialnum)], [cellstr('File Index'); num2cell(filenum)]];
    outT = cell2table(outC);
    writetable(outT, ['labelled_data/', session(sesidx).name, '_', session(sesidx).type,'.csv'], 'WriteVariableNames', false)
    
    % Find persistent features
    ntfeat = 50;
        
    if strcmp( session(sesidx).type, 'S3' )
        session(sesidx).results.persistent_S2 = intersect( session(sesidx-1).results.feat_descr(session(sesidx-1).results.indices(1:ntfeat)), session(sesidx-2).results.feat_descr(session(sesidx-2).results.indices(1:ntfeat)) );
        session(sesidx).results.persistent_S3 = intersect( feat_descr(indices(1:ntfeat)), session(sesidx-2).results.feat_descr(session(sesidx-2).results.indices(1:ntfeat)) );
        session(sesidx).results.persistent_AL = intersect( session(sesidx).results.persistent_S2, session(sesidx).results.persistent_S3 );
    end

    
end %big for loop

%%
for sesidx = 3:3:12
    disp(session(sesidx).name);
    disp('S2')
    disp(session(sesidx).results.persistent_S2);
    disp('S3')
    disp(session(sesidx).results.persistent_S3);
    disp('ALL')
    disp(session(sesidx).results.persistent_AL);
end
    
    

% %% Problem 1: Generate some nice plots
% close all;
% places = 15;
% curname = 'null';
% figdx = 0;
% handle = cell(0);
% best_feats = containers.Map();
% % iterate over all sessions
% for sesidx = 1:length(session)
%     % if we're starting a new subject
%     if ~strcmp(session(sesidx).name, curname)
%         for i = 1:length(handle) %make all the x axes the same in a subplot
%             handle{i}.XLim = [0, maxval*1.05];
%         end
%         curname = session(sesidx).name;
%         figdx = figdx+1;
%         figure(figdx);
%         clf;
%         hold on
%         subdx = 1;
%         maxval = 0;
%         handle = cell(0);
%     end
%     [sorted, indices] = sort(session(sesidx).results.fishers, 'descend');
%     handle{subdx} = subplot(3, 1, subdx);
%     values = sorted(places:-1:1);
%     if max(values) > maxval
%         maxval = max(values);
%     end
%     barh(values);
%     descriptors = session(sesidx).results.feat_descr(indices(places:-1:1));
%     if subdx == 3
%         xlabel('Fisher Score')
%     end
%     subdx = subdx+1;
%     set(gca,'yticklabel',descriptors)
%     title(['Top Fisher Scores for ' session(sesidx).name, ' ', session(sesidx).type]);
%     % Add all the highest features to a map so we can use them later
%     best_feats(char(session(sesidx).results.feat_descr(indices(1)))) = indices(1);
% end
% for i = 1:length(handle)
%     handle{i}.XLim = [0, maxval*1.05];
% end
% 
